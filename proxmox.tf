terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.9.11"
    }
  }
}

provider "proxmox" {
  pm_parallel     = 3
  pm_tls_insecure = true
  pm_api_url      = var.pm_api_url
  pm_password     = var.pm_password
  pm_user         = var.pm_user
  pm_log_enable   = false
  pm_log_file     = "terraform-plugin-proxmox.log"
  pm_log_levels = {
    _default    = "debug"
    _capturelog = ""
  }
}

### VM Analytics
resource "proxmox_vm_qemu" "osm" {

  count       = 1
  name        = "osm"
  target_node = "pve-node"
  clone       = "template-centos07"
  agent       = 1

  os_type  = "cloud-init"
  cores    = 4
  sockets  = "4"
  vcpus    = "4"
  cpu      = "host"
  memory   = 32768
  balloon  = "32768"
  scsihw   = "virtio-scsi-pci"
  bootdisk = "scsi0"

  disk {
    size    = "50G"
    type    = "scsi"
    storage = "data01"
  }

  disk {
    size    = "300G"
    type    = "scsi"
    storage = "data01"
  }

  network {
    model  = "virtio"
    bridge = "vmbr204"
  }

  lifecycle {
    ignore_changes = [
      network, id, ciuser
    ]
  }

  vga {
    type   = "virtio"
    memory = 128
  }
  # # Cloud Init Settings
  ipconfig0 = "ip=XX.XX.XX.XX/23,gw=XX.XX.XX.XX"

  ssh_user = "root"

  sshkeys = <<EOF
  ${var.ssh_key_USER}
  EOF
}

output "ip-analytics" {
   value =  resource.proxmox_vm_qemu.osm[*].default_ipv4_address
}

resource "local_file" "osm" {
  filename = "./inventory/hosts"
  content = templatefile("/opt/openstreetmap-data/ansible.tpl",
    {
      osm_ip = proxmox_vm_qemu.osm.*.default_ipv4_address
    }
  )
  provisioner "local-exec" {
    command = "sleep 90; ansible-playbook -i ./inventory/hosts ./osm.yaml -u root"
  }
}
