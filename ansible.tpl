analytics:
  hosts:
%{ for index, ip in osm_ip ~}
    osm:
      ansible_ssh_host: ${ip}
%{ endfor ~}
