## Плейбук для установки OSM сервера

0. Плейбук написан для установки vm с OSM для серверов под управлением PROXMOX. В качестве базы данных используется PostgreSQL-12. В качестве ОС используется предпоготовленный образ на базе Centos 7.9. Можно использовать ansible playbook без terraform.
  - скопировать склонированный репозиторий в /opt
  - выполнить либо:
    - terraform apply  # при использовании terraform
    - ansible-playbook -i ./inventory/hosts osm.yaml -u root # при использовании ansible без terraform

1. Скачать карты:
 - [Всей планеты](https://planet.openstreetmap.org/pbf/planet-latest.osm.pbf)
 - [Карты регионов](https://download.geofabrik.de/)

2. После установки OSM серевера необходимо импортировать карты в базу данных, для этого использовать команду:

**osm2pgsql --create --cache 16000 --number-processes 8 --hstore --multi-geometry --database gis --slim --style /usr/local/src/openstreetmap-carto/openstreetmap-carto.style --tag-transform-script /usr/local/src/openstreetmap-carto/openstreetmap-carto.lua XXXX.osm.pbf**

--**create**: Run in create mode. This is the default if -a, --append is not specified. Removes existing data from the database tables!

--**cache**: Only for slim mode: Use up to NUM MB of RAM for caching nodes. Giving osm2pgsql sufficient cache to store all imported nodes typically greatly increases the speed of the import. Each cached node requires 8 bytes of cache, plus about 10% - 30% overhead. As a rule of thumb, give a bit more than the size of the import file in PBF format. If the RAM is not big enough, use about 75% of memory. Make sure to leave enough RAM for PostgreSQL. It needs at least the amount of shared_buffers given in its configuration. Defaults to 800.

--**number-processes**: number of CPU cores on your server.

--**hstore**: add tags without column to an additional hstore (key/value) column to PostgreSQL tables

--**multi-geometry**: Normally osm2pgsql splits multi-part geometries into separate database rows per part. A single OSM object can therefore use several rows in the output tables. With this option, osm2pgsql instead generates multi-geometry features in the PostgreSQL tables.

--**database**: select database.

--**slim**: Store temporary data in the database. Without this mode, all temporary data is stored in RAM and if you do not have enough the import will not work successfully. With slim mode, you should be able to import the data even on a system with limited RAM, although if you do not have enough RAM to cache at least all of the nodes, the time to import the data will likely be greatly increased.

--**style**: specify the location of style file

--**tag-transform-script**: Specify a Lua script to handle tag filtering and normalisation. The script contains callback functions for nodes, ways and relations, which each take a set of tags and returns a transformed, filtered set of tags which are then written to the database.


3. Если необходимо объединить карты, скачайте необходимые карты регионов и  запустите скрипт: **osmium merge 1.osm.pbf 2.osm.pbf 3.osm.pbf -o merged.osm**

3.1. После импортировать карту в базу способом указанным в пункте 2.

4. После импорта карты запустить renderd: **systemctl start renderd**
