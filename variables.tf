variable "pm_api_url" {
  description = "URL proxmox node"
  type        = string
}

variable "pm_user" {
  description = "Login"
  type        = string
}

variable "pm_password" {
  description = "Password"
  type        = string
}

variable "ssh_key_lynx" {
  description = "ssh key"
  type        = string
}

variable "ssh_key_ilykhin" {
  description = "ssh key"
  type        = string
}

variable "ssh_key_sin" {
  description = "ssh key"
  type        = string
}

variable "ssh_key_andrew" {
  description = "ssh key"
  type        = string
}

variable "vms" {
  description = "vms"
  type        = string
}
